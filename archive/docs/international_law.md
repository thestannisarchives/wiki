# International Law
## Use of Force

[https://dod.defense.gov/Portals/1/Documents/pubs/DoD Law of War Manual - June 2015 Updated Dec 2016.pdf?ver=2016-12-13-172036-190](https://dod.defense.gov/Portals/1/Documents/pubs/DoD%20Law%20of%20War%20Manual%20-%20June%202015%20Updated%20Dec%202016.pdf?ver=2016-12-13-172036-190)

[https://www.springer.com/gp/book/9789067047395](https://www.springer.com/gp/book/9789067047395)

 [https://www.amazon.com/International-Law-Armed-Conflict-Contemporary/dp/1454881356/ref=sr_1_1?dchild=1&keywords=laurie+blank&qid=1608485042&s=books&sr=1-1](https://www.amazon.com/International-Law-Armed-Conflict-Contemporary/dp/1454881356/ref=sr_1_1?dchild=1&keywords=laurie+blank&qid=1608485042&s=books&sr=1-1)

**Legality of Targeted Killing Program under International Law**

[https://www.lawfareblog.com/legality-targeted-killing-program-under-international-law](https://www.lawfareblog.com/legality-targeted-killing-program-under-international-law) 

**A Summary of the White House Report on Legal Frameworks Governing Use of Military Force**

[https://www.lawfareblog.com/summary-white-house-report-legal-frameworks-governing-use-military-force](https://www.lawfareblog.com/summary-white-house-report-legal-frameworks-governing-use-military-force)

**The Legal Framework for the United States' Use of Military Force Since 9/11**

[https://www.defense.gov/Newsroom/Speeches/Speech/Article/606662/the-legal-framework-for-the-united-states-use-of-military-force-since-911/](https://www.defense.gov/Newsroom/Speeches/Speech/Article/606662/the-legal-framework-for-the-united-states-use-of-military-force-since-911/)

**Text of John Brennan's Speech on Drone Strikes at the Wilson Center**

[https://www.lawfareblog.com/text-john-brennans-speech-drone-strikes-today-wilson-center](https://www.lawfareblog.com/text-john-brennans-speech-drone-strikes-today-wilson-center)

**Jeh Johnson Speech at Yale Law School**

[https://www.lawfareblog.com/jeh-johnson-speech-yale-law-school](https://www.lawfareblog.com/jeh-johnson-speech-yale-law-school)

************************Israeli Targeted Killing Case************************

[](https://www.law.upenn.edu/institutes/cerl/conferences/targetedkilling/papers/IsraeliTargetedKillingCase.pdf)

**Characterizing US Operations in Pakistan: Is the United States Engaged In An Armed Conflict?**

[https://core.ac.uk/download/pdf/144226263.pdf](https://core.ac.uk/download/pdf/144226263.pdf)

****************************************************************************************************************************Israeli Supreme Court on Int’l Terrorism as an Armed Conflict****************************************************************************************************************************

Israeli supreme court suggests that transnational terrorism should be treated as an international armed conflict

[https://versa.cardozo.yu.edu/sites/default/files/upload/opinions/Public%20Committee%20Against%20Torture%20in%20Israel%20v.%20Government%20of%20Israel.pdf](https://versa.cardozo.yu.edu/sites/default/files/upload/opinions/Public%20Committee%20Against%20Torture%20in%20Israel%20v.%20Government%20of%20Israel.pdf)

************************************************Military Legal Resources************************************************

[https://www.loc.gov/rr/frd/Military_Law/military-legal-resources-home.html](https://www.loc.gov/rr/frd/Military_Law/military-legal-resources-home.html) great military legal resources!

**Michael N. Schmitt: PILAC Lecture on Cyber Operations and IHL**

[https://www.youtube.com/watch?v=ZWwrVAMSOT4](https://www.youtube.com/watch?v=ZWwrVAMSOT4)

**The UN Gaza Report: Heads I Win, Tails You Lose**

[https://www.lawfareblog.com/un-gaza-report-heads-i-win-tails-you-lose](https://www.lawfareblog.com/un-gaza-report-heads-i-win-tails-you-lose)

**********************************************LOAC and Military Operations: Comprehensive Overviews**********************************************

[https://archive.org/details/geoffrey-s.-corn-rachel-e.-van-landingham-shane-r.-reeves-eds.-u.-s.-military-op_202102/mode/2up](https://archive.org/details/geoffrey-s.-corn-rachel-e.-van-landingham-shane-r.-reeves-eds.-u.-s.-military-op_202102/mode/2up)

[https://archive.org/details/fleck-dieter-gill-terry-d-the-handbook-of-the-international-law-of-military-oper/mode/2up](https://archive.org/details/fleck-dieter-gill-terry-d-the-handbook-of-the-international-law-of-military-oper/mode/2up)

[https://archive.org/details/daragh-murray-elizabeth-wilmshurst-francoise-hampson-charles-garraway-noam-lubel/mode/2up](https://archive.org/details/daragh-murray-elizabeth-wilmshurst-francoise-hampson-charles-garraway-noam-lubel/mode/2up)

‘************************************Highway of Death’************************************

The 'Highway of Death' is not an example of a war crime carried out by the USA, see:
[https://ihl-databases.icrc.org/customary-ihl/eng/docs/v2_cou_us_rule47](https://ihl-databases.icrc.org/customary-ihl/eng/docs/v2_cou_us_rule47) (towards the bottom)

********************************Soleimani Strike********************************

Resources Regarding the Soleimani Strike:
[https://assets.documentcloud.org/documents/6808252/DOD-GC-Speech-BYU-QS.pdf](https://assets.documentcloud.org/documents/6808252/DOD-GC-Speech-BYU-QS.pdf) - legal
[https://crsreports.congress.gov/product/pdf/R/R46148](https://crsreports.congress.gov/product/pdf/R/R46148) Q&A
[https://www.lawfareblog.com/was-soleimani-killing-assassination](https://www.lawfareblog.com/was-soleimani-killing-assassination)[https://www.treasury.gov/press-center/press-releases/Pages/hp644.aspx](https://www.treasury.gov/press-center/press-releases/Pages/hp644.aspx)

******************************Other Resources******************************

Some other scholarly papers by world-leading experts:

*Investigating Violations of International Law in Armed Conflict*

[https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1683980](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1683980)
^ Great read from legal scholar Mike Schmitt, if you'd like to better your understanding regarding "War crimes." 

[https://www.reading.ac.uk/law/Staff/m-schmitt.aspx](https://www.reading.ac.uk/law/Staff/m-schmitt.aspx) 
^ All of Michael Schmitt's papers, books, etc. can be found here.

*'Game of Thrones' Reflections on International Relations*[https://www.rand.org/blog/2019/04/game-of-thrones-reflections-on-international-relations.html](https://www.rand.org/blog/2019/04/game-of-thrones-reflections-on-international-relations.html)
^ A personal favorite.

*A Guide To The Basics Of International Law*[https://www.law.georgetown.edu/wp-content/uploads/2019/08/A-Guide-to-the-Basics-of-Intl-Law.pdf](https://www.law.georgetown.edu/wp-content/uploads/2019/08/A-Guide-to-the-Basics-of-Intl-Law.pdf)
^ A short read (6 pages)

[https://jnslp.com/wp-content/uploads/2017/04/Totality_of_the_Circumstances_FINAL.pdf](https://jnslp.com/wp-content/uploads/2017/04/Totality_of_the_Circumstances_FINAL.pdf)

*Totality of the Circumstances: The DoD Law ofWar Manual and the Evolving Notion of Direct Participation in Hostilities*

********************************************************On the International Criminal Court********************************************************

[https://www.heritage.org/report/the-us-should-not-join-the-international-criminal-court](https://www.heritage.org/report/the-us-should-not-join-the-international-criminal-court)

> The U.S. has refused to join the ICC because it lacks prudent safeguards against political manipulation, possesses sweeping authority without accountability to the U.N. Security Council, and violates national sovereignty by claiming jurisdiction over the nationals and military personnel of non-party states in some circumstances.
> 

*Jus in Bello Futura ignotus: The United States, the International Criminal Court, and the uncertain future of the Law of Armed Conflict*

[https://web.archive.org/web/20170131093317/https://www.jagcnet.army.mil/DOCLIBS/MILITARYLAWREVIEW.NSF/20a66345129fe3d885256e5b00571830/7caab5aad6a5fea585257f33004ed107/$FILE/7.  By Lieutenant Colonel James T. Hill.pdf](https://web.archive.org/web/20170131093317/https://www.jagcnet.army.mil/DOCLIBS/MILITARYLAWREVIEW.NSF/20a66345129fe3d885256e5b00571830/7caab5aad6a5fea585257f33004ed107/$FILE/7.%20%20By%20Lieutenant%20Colonel%20James%20T.%20Hill.pdf) 

Details issues with the ICC's flawed interpretations of LOAC, and what they mean for the future of war, and also violation on individual states and how they deal with their own.

C*onducting unconventional warfare in compliance with the Law of Armed Conflict*

[https://tjaglcspublic.army.mil/documents/27431/2115093/View+the+PDF/2155811d-648e-4c54-bd16-db50048fa9be](https://tjaglcspublic.army.mil/documents/27431/2115093/View+the+PDF/2155811d-648e-4c54-bd16-db50048fa9be)

*How the International Criminal Court Threatens Treaty Norms*

[https://tjaglcspublic.army.mil/documents/27431/61281/Newton+Article+on+ICC+Jurisdiction.pdf/a5f9f672-3846-4967-a85e-d434a84b464b](https://tjaglcspublic.army.mil/documents/27431/61281/Newton+Article+on+ICC+Jurisdiction.pdf/a5f9f672-3846-4967-a85e-d434a84b464b)

*Exercise Jurisdiction at the Edge—What Happens Next? An Analysis of International Criminal Court Substantive Law as Applied to Non-Party State Nationals*

[https://tjaglcspublic.army.mil/documents/27431/16066286/2020-Issue-3-Exercising+Jurisdiction+at+the+Edge.pdf/6863edf3-9e30-450a-89e2-eddf45861ecc](https://tjaglcspublic.army.mil/documents/27431/16066286/2020-Issue-3-Exercising+Jurisdiction+at+the+Edge.pdf/6863edf3-9e30-450a-89e2-eddf45861ecc)

****************************************************************NATO’s Role in Peace Operations****************************************************************

*NATO's role in peace operations: Reexamining the treaty after Bosnia and Kosovo*

p. 10

[https://www.loc.gov/rr/frd/Military_Law/Military_Law_Review/pdf-files/275081~1.pdf](https://www.loc.gov/rr/frd/Military_Law/Military_Law_Review/pdf-files/275081~1.pdf)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled.png)

## International Humanitarian Law

*Special Operations Commando Raids and Enemy Hors de Combat*

p.37

*The Detention and Prosecution of Insurgents and Other Non-Traditional Combatants— A Look at the Task Force 134 Process and the Future of Detainee Prosecutions*

p. 74

[https://www.jag.navy.mil/documents/navylawreview/ALSJSEditionJuly2007.pdf](https://www.jag.navy.mil/documents/navylawreview/ALSJSEditionJuly2007.pdf)

*Protecting civilians in urban areas: A military perspective on the application of international humanitarian law* 

[https://international-review.icrc.org/sites/default/files/irc_97_901-11.pdf](https://international-review.icrc.org/sites/default/files/irc_97_901-11.pdf)

**Introduction to Defense Institute of International Legal Studies**

[https://www.youtube.com/watch?v=zEpHRVqEgs4](https://www.youtube.com/watch?v=zEpHRVqEgs4)

**Danish Law of War Manual**

[https://forsvaret.dk/globalassets/fko---forsvaret/dokumenter/publikationer/-military-manual-updated-2020-2.pdf](https://forsvaret.dk/globalassets/fko---forsvaret/dokumenter/publikationer/-military-manual-updated-2020-2.pdf)

Less of a treatise than the US version, however, easier to read and neat graphics to understand complex mattes, like International Armed Conflict vs Non-International Armed Conflict. (p.42)

**Human Shields**

> The tactic involves placing noncombatants at or near likely military and strategic facilities to deter an attacker with the threat of collateral casualties to civilians or prisoners of war (POWs)
> 

^

[https://fas.org/irp/cia/product/iraq_human_shields/iraq_human_shields.pdf](https://fas.org/irp/cia/product/iraq_human_shields/iraq_human_shields.pdf)

[https://poseidon01.ssrn.com/delivery.php?ID=301002120071066114064126066098110105032027023067011038006025013072078075068031098101021018111115103010043007029091106127098083098074087007053117099115005106002072091026021066068125022091069086070001010104003030029003090026019107070088024003075116067093&EXT=pdf&INDEX=TRUE](https://poseidon01.ssrn.com/delivery.php?ID=301002120071066114064126066098110105032027023067011038006025013072078075068031098101021018111115103010043007029091106127098083098074087007053117099115005106002072091026021066068125022091069086070001010104003030029003090026019107070088024003075116067093&EXT=pdf&INDEX=TRUE)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%201.png)

[The Application of IHL in the Goldstone Report: A Critical Commentary](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1596214)

**Classifying the Conflict in Syria**

[https://digital-commons.usnwc.edu/cgi/viewcontent.cgi?article=1691&context=ils](https://digital-commons.usnwc.edu/cgi/viewcontent.cgi?article=1691&context=ils)

******************************Starter Readings on LOAC******************************

*Historical Evolution of Laws on Armed Conflicts: A Jurisprudential Perspective from Geneva Conventions*

[https://poseidon01.ssrn.com/delivery.php?ID=591008092002066089112122088027075024057083047011093057124027025005120008070081126076006012033059126097000027124127076117001010019037091041086073067102025064108094091058061002117100031071080122118023127112123025101003101028101093103113085109109089029090&EXT=pdf&INDEX=TRUE](https://poseidon01.ssrn.com/delivery.php?ID=591008092002066089112122088027075024057083047011093057124027025005120008070081126076006012033059126097000027124127076117001010019037091041086073067102025064108094091058061002117100031071080122118023127112123025101003101028101093103113085109109089029090&EXT=pdf&INDEX=TRUE)

[https://www.icrc.org/en/doc/assets/files/publications/icrc-002-0361.pdf](https://www.icrc.org/en/doc/assets/files/publications/icrc-002-0361.pdf)
^ This 1862 seminal book, inspired the creation of the Geneva conventions and their subsequent protocols, and the creation of the ICRC.

*Historical Evolution Of Laws On Armed Conflicts: Perspectives Of Geneva Conventions*

[https://yubarajsangroula.com.np/assets/uploads/b64bc-geneva-convention.pdf](https://yubarajsangroula.com.np/assets/uploads/b64bc-geneva-convention.pdf)

*Understanding the International Criminal Court*

[https://www.icc-cpi.int/sites/default/files/Publications/understanding-the-icc.pdf](https://www.icc-cpi.int/sites/default/files/Publications/understanding-the-icc.pdf)
"The International Criminal Court is not a substitute for national courts. According to the Rome Statute, it is the duty of every State to exercise its criminal jurisdiction over those responsible for international crimes. The International Criminal Court can ***only*** intervene where a State is ***unable*** or ***unwilling*** genuinely to carry out the investigation and prosecute the perpetrators"

*Targeting: Principles of International Humanitarian Law*

[https://web.archive.org/web/20190113065309/https://law.yale.edu/system/files/area/center/global/document/gross_targeting_and_civilians_directly_participating_in_hostilities.pdf](https://web.archive.org/web/20190113065309/https://law.yale.edu/system/files/area/center/global/document/gross_targeting_and_civilians_directly_participating_in_hostilities.pdf)

## US Policy & Doctrine

**Leahy Law Fact Sheet - United States Department of State**

[https://www.state.gov/key-topics-bureau-of-democracy-human-rights-and-labor/human-rights/leahy-law-fact-sheet/](https://www.state.gov/key-topics-bureau-of-democracy-human-rights-and-labor/human-rights/leahy-law-fact-sheet/)

**US Army doctrine on protection of civilians, 2015**
[https://fas.org/irp/doddir/army/atp3-07-6.pdf](https://fas.org/irp/doddir/army/atp3-07-6.pdf)

**Feb 2002 letter from WH confirming that detainees must be humanely treated per international and domestic law, as well as US values.**
[https://www.esd.whs.mil/Portals/54/Documents/FOID/Reading Room/Detainne_Related/Humane_Treatment_of_alQaeda_and_Taliban_Detainees_08F0130_Final.pdf](https://www.esd.whs.mil/Portals/54/Documents/FOID/Reading%20Room/Detainne_Related/Humane_Treatment_of_alQaeda_and_Taliban_Detainees_08F0130_Final.pdf)

*The Evolution of Forward Surgery in the US Army From the Revolutionary War to the Combat Operations of the 21st Century*

[https://ckapfwstor001.blob.core.usgovcloudapi.net/pfw-images/dbimages/FStoWeb.pdf](https://ckapfwstor001.blob.core.usgovcloudapi.net/pfw-images/dbimages/FStoWeb.pdf)

*Out of the Crucible: How the US Military Transformed Combat Casualty Care in Iraq and Afghanistan*

[https://ckapfwstor001.blob.core.usgovcloudapi.net/pfw-images/borden/crucible/Crucible.pdf](https://ckapfwstor001.blob.core.usgovcloudapi.net/pfw-images/borden/crucible/Crucible.pdf)

[https://twitter.com/StateDept/status/1415204416777453571](https://twitter.com/StateDept/status/1415204416777453571)

*The House built on sand: An analysis of battlefield mercy killings in Non-International Armed Conflicts under International Humanitarian Law and International Human Rights Law*

[https://www.jag.navy.mil/documents/navylawreview/NLR66_Sham.pdf](https://www.jag.navy.mil/documents/navylawreview/NLR66_Sham.pdf)

**Digest of United States Practice in International Law**

[https://www.state.gov/digest-of-united-states-practice-in-international-law/](https://www.state.gov/digest-of-united-states-practice-in-international-law/)

[https://brill.com/view/book/edcoll/9789004404601/BP000005.xml](https://brill.com/view/book/edcoll/9789004404601/BP000005.xml)

*The crime of aggression: The United States Perspective*

[https://digitalcommons.law.yale.edu/cgi/viewcontent.cgi?article=6014&context=fss_papers](https://digitalcommons.law.yale.edu/cgi/viewcontent.cgi?article=6014&context=fss_papers)

[https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC1681630/](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC1681630/)

[NCBI - WWW Error Blocked Diagnostic](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC1681630/)

**DOJ response to ABA task force on detention ops**

[](https://www.justice.gov/sites/default/files/olc/legacy/2010/01/08/memo-aba-taskforce.pdf)

***The legitimacy of modern conventional weaponry***

[https://tjaglcspublic.army.mil/documents/27431/2251871/View+the+PDF/ee82f732-b0b4-4275-b230-5be04ad16474](https://tjaglcspublic.army.mil/documents/27431/2251871/View+the+PDF/ee82f732-b0b4-4275-b230-5be04ad16474) 

p. 95

Examines Napalm, clusters, etc.

***Protection of civilians—military reference guide***

[https://publications.armywarcollege.edu/pubs/3520.pdf](https://publications.armywarcollege.edu/pubs/3520.pdf)

## Cyber

***Responding to Proxy  Cyber Operations  Under International Law***

[https://cyberdefensereview.army.mil/Portals/6/Documents/2021_fall/02_Johnson_Schmitt_CDR_V6N4-Fall_2021.pdf?ver=UclHQrn_coYfGcPXfimQcA%3D%3D](https://cyberdefensereview.army.mil/Portals/6/Documents/2021_fall/02_Johnson_Schmitt_CDR_V6N4-Fall_2021.pdf?ver=UclHQrn_coYfGcPXfimQcA%3d%3d)

**Cyber Law Toolkit**

[https://cyberlaw.ccdcoe.org/wiki/Main_Page](https://cyberlaw.ccdcoe.org/wiki/Main_Page)

**Down Is Not Always Out: Hors De Combat in the Close Fight**

[https://lieber.westpoint.edu/down-not-always-out-hors-de-combat-close-fight/](https://lieber.westpoint.edu/down-not-always-out-hors-de-combat-close-fight/)

**CCDCOE - The NATO Cooperative Cyber Defence Centre of Excellence**

[https://ccdcoe.org/](https://ccdcoe.org/)

**New Technologies and the Law in War and Peace**

[https://www.cambridge.org/au/academic/subjects/law/humanitarian-law/new-technologies-and-law-war-and-peace?format=PB&isbn=9781108740128](https://www.cambridge.org/au/academic/subjects/law/humanitarian-law/new-technologies-and-law-war-and-peace?format=PB&isbn=9781108740128)

**Tallinn Manual 2.0 on the International Law Applicable to Cyber Operations**

[https://www.cambridge.org/au/academic/subjects/law/humanitarian-law/tallinn-manual-20-international-law-applicable-cyber-operations-2nd-edition?format=PB](https://www.cambridge.org/au/academic/subjects/law/humanitarian-law/tallinn-manual-20-international-law-applicable-cyber-operations-2nd-edition?format=PB)

**The U.S. Landmine Policy Complies with International Law**

[https://lieber.westpoint.edu/u-s-landmine-policy-complies-international-law/](https://lieber.westpoint.edu/u-s-landmine-policy-complies-international-law/)

***Operational Law Handbook* (2021)**

[https://tjaglcs.army.mil/documents/35956/56931/2021+Operational+Law+Handbook.pdf](https://tjaglcs.army.mil/documents/35956/56931/2021+Operational+Law+Handbook.pdf)

**The Commander's Handbook on the Law of Naval Operations (2022)**

[https://usnwc.libguides.com/ld.php?content_id=66281931](https://usnwc.libguides.com/ld.php?content_id=66281931)

> commonly called the Rendulic Rule, the law of armed conflict recognizes
that commanders must assess the military necessity of an action based on the information available to them at the relevant time. They cannot be judged based on information that subsequently comes to light.
> 

**On Directly Participating in Hostilities (DPH)**

[https://web.archive.org/web/20200717132306/https://nyujilp.org/wp-content/uploads/2012/04/42.3-Watkin.pdf](https://web.archive.org/web/20200717132306/https://nyujilp.org/wp-content/uploads/2012/04/42.3-Watkin.pdf)

[https://web.archive.org/web/20190830170604/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Schmitt.pdf](https://web.archive.org/web/20190830170604/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Schmitt.pdf)

[https://web.archive.org/web/20200720052330/https://nyujilp.org/wp-content/uploads/2012/04/42.3-Boothby.pdf](https://web.archive.org/web/20200720052330/https://nyujilp.org/wp-content/uploads/2012/04/42.3-Boothby.pdf)

[https://web.archive.org/web/20200717133617/https://nyujilp.org/wp-content/uploads/2012/04/42.3-Parks.pdf](https://web.archive.org/web/20200717133617/https://nyujilp.org/wp-content/uploads/2012/04/42.3-Parks.pdf)

[https://jnslp.com/wp-content/uploads/2017/04/Totality_of_the_Circumstances_FINAL.pdf](https://jnslp.com/wp-content/uploads/2017/04/Totality_of_the_Circumstances_FINAL.pdf)

[https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1604626](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1604626)

[https://law.yale.edu/sites/default/files/area/center/global/document/gross_targeting_and_civilians_directly_participating_in_hostilities.pdf](https://law.yale.edu/sites/default/files/area/center/global/document/gross_targeting_and_civilians_directly_participating_in_hostilities.pdf)

[https://twitter.com/Articles_of_War/status/1532334700781608960](https://twitter.com/Articles_of_War/status/1532334700781608960)

> Attack effects can offer incomplete or even misleading impressions of legality. @cornjag1 & Sean Watts argue the focal point of inquiry related to targeting operations must be the attack judgement, not the attack outcome.
> 

**THE GOLDSTONE REPORT: POLITICIZATION OF THE LAW OF ARMED CONFLICT AND THOSE LEFT BEHIND**

[https://tjaglcspublic.army.mil/documents/27431/1415913/View+the+PDF/6ea296ee-ce2a-425b-a360-314a67395031](https://tjaglcspublic.army.mil/documents/27431/1415913/View+the+PDF/6ea296ee-ce2a-425b-a360-314a67395031)

**On Iraq war crimes**

[https://core.ac.uk/download/pdf/216993993.pdf](https://core.ac.uk/download/pdf/216993993.pdf) 

**From 'The War in Iraq: A Legal Analysis’**

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%202.png)

***Yes, we can: The authority to detain as customary international law***

[https://tile.loc.gov/storage-services/service/ll/llmlp/58062115_202-winter-2009/58062115_202-winter-2009.pdf](https://tile.loc.gov/storage-services/service/ll/llmlp/58062115_202-winter-2009/58062115_202-winter-2009.pdf)

**BANGLADESH RAPID ACTION BATTALION: SATISFYING THE REQUIREMENTS OF THE LEAHY AMENDMENT WITH A RULE OF LAW APPROACH**
[https://tile.loc.gov/storage-services/service/ll/llmlp/58062115_215-spring-2013/58062115_215-spring-2013.pdf](https://tile.loc.gov/storage-services/service/ll/llmlp/58062115_215-spring-2013/58062115_215-spring-2013.pdf)

[Office of General Counsel > Law of War > Practice Documents](https://ogc.osd.mil/Law-of-War/Practice-Documents/)

**U.S. Position on the U.N. Convention on the Law of the Sea**
[https://digital-commons.usnwc.edu/cgi/viewcontent.cgi?article=2949&context=ils](https://digital-commons.usnwc.edu/cgi/viewcontent.cgi?article=2949&context=ils)

[THE RENDULIC ‘RULE’: MILITARY NECESSITY, COMMANDER'S KNOWLEDGE, AND METHODS OF WARFARE* | Yearbook of International Humanitarian Law | Cambridge Core](https://www.cambridge.org/core/journals/yearbook-of-international-humanitarian-law/article/abs/the-rendulic-rule-military-necessity-commanders-knowledge-and-methods-of-warfare/BEC1C29D7195FD5FD2A32BB2477DC54C)

[Is attacking the electricity infrastructure used by civilians always a war crime?](https://sites.duke.edu/lawfire/2022/10/27/is-attacking-the-electricity-infrastructure-used-by-civilians-always-a-war-crime/)

## Airstrikes & Laws of War

*********************************************Overviews on specific and general airstrikes, the ROEs, LOAC, and policy that govern them, with an emphasis on the US.*********************************************

### Myth and Misinformation

**Drone Papers by the Intercept Debunked**

[https://www.lawfareblog.com/drone-papers-intercepting-nonsense](https://www.lawfareblog.com/drone-papers-intercepting-nonsense)

[https://www.lawfareblog.com/what-intercept-found-drone-papers—and-what-i-found-them](https://www.lawfareblog.com/what-intercept-found-drone-papers%E2%80%94and-what-i-found-them)

[https://www.lawfareblog.com/response-drone-papers-aumf-targeting-deliberate-process-robust-political-accountability](https://www.lawfareblog.com/response-drone-papers-aumf-targeting-deliberate-process-robust-political-accountability)

**"US Led Drone Strikes Killed Innocents 90% Of The Time"**

So this "90% of Drone Strikes killed civilians" fable is from the Drone Papers which was published by the intercept (link above) which states:

> "Over a five-month period, U.S. forces used drones and other aircraft to kill 155 people in northeastern Afghanistan. They achieved 19 jackpots. Along the way, they killed at least 136 other people, all of whom were classified as EKIA, or enemies killed in action.
> 
> 
> *This means that almost 9 out of 10 people killed in these strikes were not the intended targets."*
> 

a leaked document, that the intercept interestingly cites, debunks the notion that Operation Haymaker in Afghanistan had a 90% CIVCAS rate, see the final page of the doc below. it clearly shows that just 0.67% of the strikes had any incidents involving CIVCAS (civilian casualties).
([https://www.documentcloud.org/documents/2460857-operation-haymaker-afghanistan-documents.html](https://www.documentcloud.org/documents/2460857-operation-haymaker-afghanistan-documents.html))

**"All Military Aged Males are Marked as Combatants Until Proven Otherwise"**

Nope.

[https://www.dni.gov/files/documents/Newsroom/Press%20Releases/DNI+Release+on+CT+Strikes+Outside+Areas+of+Active+Hostilities.PDF](https://www.dni.gov/files/documents/Newsroom/Press%20Releases/DNI+Release+on+CT+Strikes+Outside+Areas+of+Active+Hostilities.PDF)

> The term “non-combatant” does not include an individual who is part of a belligerent party to an armed conflict, an individual who is taking a direct part in hostilities, or an individual who is targetable in the exercise of U.S. national self-defense. Males of military age
> 
> 
> *may be*
> 
> it is not the case that all military-aged males in the vicinity of a target are deemed to be combatants.
> 

[https://endlessstratos.wordpress.com/2020/06/23/90-percent-drone-fake/](https://endlessstratos.wordpress.com/2020/06/23/90-percent-drone-fake/)

**Doctors Without Borders Air Strike**

Summary of the incident:

[https://www.stripes.com/polopoly_fs/1.380589.1448475145!/menu/standard/file/25%20Nov%20-%20GEN%20Campbell%20statement%20on%20the%20Kunduz%20MSF%20Hospital%20Investigation....pdf](https://www.stripes.com/polopoly_fs/1.380589.1448475145!/menu/standard/file/25%20Nov%20-%20GEN%20Campbell%20statement%20on%20the%20Kunduz%20MSF%20Hospital%20Investigation....pdf)

An AR 15-6 Investigation, detailed events as they went down, very comprehensive.
[http://fpp.cc/wp-content/uploads/01.-AR-15-6-Inv-Rpt-Doctors-Without-Borders-3-Oct-15_CLEAR.pdf](http://fpp.cc/wp-content/uploads/01.-AR-15-6-Inv-Rpt-Doctors-Without-Borders-3-Oct-15_CLEAR.pdf)

> The report determined that the U.S. strike upon the MSF Trauma Center in Kunduz City,
Afghanistan, was the direct result of human error, compounded by systems and
procedural failures. The U.S. forces directly involved in this incident did not know
the targeted compound was the MSF Trauma Center.
> 

> All members of both the ground force and the AC-130U aircrew were completely unaware the aircrew was firing on a hospital throughout the course of the engagement.
> 

******Critiques of NGO Reporting on Drones******

CIVCAS estimates from TBIJ et al are untrustworthy and inflated. They don't provide *real* evidence. They rely on post hoc reasoning and tend to be incorrect, BTFO'd by  law EXPERTS, and leaked + unclassified/declassified documents.

1. They don't know what happened before or during the strike
2. They may assume that every air operation in the region, is USA because the US conducts operations in those regions.
3. They rely on flawed eyewitness accounts (see the links below)
4. They aren't experts ([https://www.lawfareblog.com/flawed-human-rights-watch-report-gaza](https://www.lawfareblog.com/flawed-human-rights-watch-report-gaza))

Here are some critiques on NGOs methodology, numbers, etc.

https://medium.com/war-is-boring/dear-amnesty-international-do-you-even-know-how-drones-work-56b89ee705b2

https://smallwarsjournal.com/jrnl/art/drones-versus-their-critics-a-victory-for-president-obama’s-war-powers-legacy

https://www.washingtonpost.com/news/monkey-cage/wp/2014/10/06/ethical-and-methodological-issues-in-assessing-drones-civilian-impacts-in-pakistan/?arc404=true

**Wikileaks 'Collateral Murder':**

An investigation shortly after the incident concluded that US forces were unaware of the presence of the Reuters team and believed they were engaging armed insurgents.

[https://assets.documentcloud.org/documents/1506533/30-2nd-brigade-combat-team-15-6-investigation-pdf.pdf](https://assets.documentcloud.org/documents/1506533/30-2nd-brigade-combat-team-15-6-investigation-pdf.pdf)

Chain of events:

[https://military.wikia.org/wiki/July_12,_2007_Baghdad_airstrike#Military_legal_review](https://military.wikia.org/wiki/July_12,_2007_Baghdad_airstrike#Military_legal_review)

### Law (Domestic and International) and Policy Governing Dronestrikes

1. **DOJ's May 25, 2011. White Paper on Legality of a Lethal Operations by the CIA against a US Citizen** - ([https://cryptome.org/2014/09/doj-2011-targeting-citizens-nyt-14-0909.pdf](https://cryptome.org/2014/09/doj-2011-targeting-citizens-nyt-14-0909.pdf))
2. **2014 DoD Report to Congress on Process for Determining Targets of Lethal or Capture Operations** - ([https://www.aclu.org/sites/default/files/field_document/53-2._exhibit_52_-_report_on_process_12.1.15.pdf](https://www.aclu.org/sites/default/files/field_document/53-2._exhibit_52_-_report_on_process_12.1.15.pdf))
3. **DOJ July 16, 2010, memorandum, Re: Applicability Of Federal Criminal Laws and the Constitution to Contemplated Lethal Operations Against Shaykh Anwar al-Aulaqi** - ([https://fas.org/irp/agency/doj/olc/aulaqi.pdf](https://fas.org/irp/agency/doj/olc/aulaqi.pdf))
4. **DOJ White Paper, Lawfulness of a Lethal Operation Directed Against a U.S. Citizen Who Is a Senior Operational Leader Of Al-Qaida or An Associated Force** - ([https://fas.org/irp/eprint/doj-lethal.pdf](https://fas.org/irp/eprint/doj-lethal.pdf))

### Efficacy of Dronestrikes

Now, are they effective? Answer: A clear yes! See:

1. **Patrick B. Johnston, Anoop K. Sarbahi: The Impact of US Drone Strikes on Terrorism in Pakistan. International Studies Quarterly,
Volume 60, Issue 2 1 June 2016, Pages 203-21** - ([https://experts.umn.edu/en/publications/the-impact-of-us-drone-strikes-on-terrorism-in-pakistan](https://experts.umn.edu/en/publications/the-impact-of-us-drone-strikes-on-terrorism-in-pakistan))
2. **Aqil Shah, Do US. Drone Strikes Cause Blowback? Evidence from Pakistan and Beyond, International Security
Volume 42 Issue 04 Spring 2018 p.47-84** - ([https://www.mitpressjournals.org/doi/abs/10.1162/isec_a_00312?platform=hootsuite&mobileUi=0](https://www.mitpressjournals.org/doi/abs/10.1162/isec_a_00312?platform=hootsuite&mobileUi=0)) 
3. **Bryan C. Price. Targeting Top Terrorists: How Leadership Decapitation Contributes to Counterterrorism:
Quarterly Journal: International Security, vol. 4. no. 36.** - ([https://www.belfercenter.org/publication/targeting-top-terrorists-how-leadership-decapitation-contributes-counterterrorism](https://www.belfercenter.org/publication/targeting-top-terrorists-how-leadership-decapitation-contributes-counterterrorism))
4. **Report: ISR Support to Small Footprint CT Operations - Somalia/Yemen, ISR Task Force Reg and Analysis Division Feb 2013** - ([https://goodtimesweb.org/overseas-war/2015/small-footprint-operations-may-2013.pdf](https://goodtimesweb.org/overseas-war/2015/small-footprint-operations-may-2013.pdf))

### Relevant Legal Treatises & Textbooks

1. **Geoffrey S. Com. Rachel E. Van Landingham et al; Military Operations, Law, policy, and practice, 2015** - ([https://www.amazon.com/U-S-Military-Operations-Policy-Practice/dp/0190456639](https://www.amazon.com/U-S-Military-Operations-Policy-Practice/dp/0190456639)) 

2. **William H. Boothby and Michael N. Schmitt The Law of Targeting, 2012** - ([https://global.oup.com/academic/product/the-law-of-targeting-9780199696611?cc=us&lang=en&](https://global.oup.com/academic/product/the-law-of-targeting-9780199696611?cc=us&lang=en&)) 

3. **Terry D. Gill and Dieter Fleck; The Handbook of the International Law of Military Operations 2nd edition, 2016** - ([https://global.oup.com/academic/product/the-handbook-of-the-international-law-of-military-operations-9780198744627?cc=us&lang=en&](https://global.oup.com/academic/product/the-handbook-of-the-international-law-of-military-operations-9780198744627?cc=us&lang=en&)) 
4) **William Boothby, Weapons and the Law of Armed Conflict 2nd Edition, 2016** - ([https://global.oup.com/academic/product/weapons-and-the-law-of-armed-conflict-9780198728504?cc=us&lang=en&](https://global.oup.com/academic/product/weapons-and-the-law-of-armed-conflict-9780198728504?cc=us&lang=en&))

*Credit to **@The fuck is this vc** for the learning material above.*

**Signature Strikes and Double Taps:**

Double Taps don't exist.

*There are no SOPs, or doctrines, etc. from the US Government, declassified/unclassified or leaked, that supports the claim.*

Signature Strikes:

*Multiple Forms of Intelligence Must Corroborate the Target:*

[https://goodtimesweb.org/overseas-war/2015/small-footprint-operations-may-2013.pdf](https://goodtimesweb.org/overseas-war/2015/small-footprint-operations-may-2013.pdf)

[https://i.imgur.com/5DTyi3g.png](https://i.imgur.com/5DTyi3g.png)

[https://fas.org/irp/offdocs/ppd/ppg-procedures.pdf](https://fas.org/irp/offdocs/ppd/ppg-procedures.pdf)

[https://i.imgur.com/5s1jnyw.png](https://i.imgur.com/5s1jnyw.png)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%203.png)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%204.png)

**Drones: Actually the Most Humane Form of Warfare Ever**

[https://www.theatlantic.com/international/archive/2013/08/drones-actually-the-most-humane-form-of-warfare-ever/278746/](https://www.theatlantic.com/international/archive/2013/08/drones-actually-the-most-humane-form-of-warfare-ever/278746/)

**Drones versus their Critics: A Victory for President Obama’s War Powers Legacy**

[https://smallwarsjournal.com/jrnl/art/drones-versus-their-critics-a-victory-for-president-obama’s-war-powers-legacy](https://smallwarsjournal.com/jrnl/art/drones-versus-their-critics-a-victory-for-president-obama%E2%80%99s-war-powers-legacy)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%205.png)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%206.png)

![Notable Screenshots, from a 2018 JCS review on CIVCAS during OIR from USAFRICOM and USCENTCOM operations.](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%207.png)

Notable Screenshots, from a 2018 JCS review on CIVCAS during OIR from USAFRICOM and USCENTCOM operations.

******************Rendulic Rule******************

[https://www.loc.gov/rr/frd/Military_Law/pdf/operational-law-handbook_2020.pdf](https://www.loc.gov/rr/frd/Military_Law/pdf/operational-law-handbook_2020.pdf) - Rendulic Rule.

![Rendulic Rule](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%208.png)

Rendulic Rule

[Search results from Military Legal Resources, Available Online](https://www.loc.gov/rr/frd/Military_Law/Military_Law_Review/pdf-files/209-fall-2011.pdf)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%209.png)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%2010.png)

**Joint Chiefs of Staff Report: "Reducing and Mitigating Civilian Casualties: Enduring Lessons”**

[https://www.documentcloud.org/documents/4318095-CIVCAS-Enduring-Lessons-1.html](https://www.documentcloud.org/documents/4318095-CIVCAS-Enduring-Lessons-1.html)

An example of great accountability and efforts

********************************************************************Setting the Record Straight on Drones, Targeted Killings, and Extrajudicial Killings********************************************************************

[https://medium.com/@JSlate__/setting-the-record-straight-on-drones-targeted-killings-and-extrajudicial-killings-60cf6e57df48](https://medium.com/@JSlate__/setting-the-record-straight-on-drones-targeted-killings-and-extrajudicial-killings-60cf6e57df48)

[Setting the record Straight on Drones, Targeted Killings, Civilian Casualties and “Extrajudicial…](https://medium.com/@JSlate__/setting-the-record-straight-on-drones-targeted-killings-and-extrajudicial-killings-60cf6e57df48)

**AFCENT AirPower Summaries 2007-2014**

[https://docs.google.com/spreadsheets/d/1_jC3HtcisjmT1fVe3UG9kEW_jUiDKurVvydlm7Y4uHg/pubhtml?gid=1722815707&single=true](https://docs.google.com/spreadsheets/d/1_jC3HtcisjmT1fVe3UG9kEW_jUiDKurVvydlm7Y4uHg/pubhtml?gid=1722815707&single=true)

**Legality of Targeted Killing Program under International Law:** [https://www.lawfareblog.com/legality-targeted-killing-program-under-international-law](https://www.lawfareblog.com/legality-targeted-killing-program-under-international-law)

**A Summary of the White House Report on Legal Frameworks Governing Use of Military Force:** [https://www.lawfareblog.com/summary-white-house-report-legal-frameworks-governing-use-military-force](https://www.lawfareblog.com/summary-white-house-report-legal-frameworks-governing-use-military-force)

**The Legal Framework for the United States' Use of Military Force Since 9/11:** [https://www.defense.gov/Newsroom/Speeches/Speech/Article/606662/the-legal-framework-for-the-united-states-use-of-military-force-since-911/](https://www.defense.gov/Newsroom/Speeches/Speech/Article/606662/the-legal-framework-for-the-united-states-use-of-military-force-since-911/)

**Text of John Brennan's Speech on Drone Strikes Today at the Wilson Center:** [https://www.lawfareblog.com/text-john-brennans-speech-drone-strikes-today-wilson-center](https://www.lawfareblog.com/text-john-brennans-speech-drone-strikes-today-wilson-center)

**Jeh Johnson Speech at Yale Law School:** [https://www.lawfareblog.com/jeh-johnson-speech-yale-law-school](https://www.lawfareblog.com/jeh-johnson-speech-yale-law-school)**Protection of Civilians (Department of Army):** [https://info.publicintelligence.net/USArmy-ProtectionCivilians.pdf](https://info.publicintelligence.net/USArmy-ProtectionCivilians.pdf)

**Civilian Casualty Mitigation Summary of Lessons, Observations and Tactics, Techniques and Procedures from Marine Expeditionary Brigade:** [https://info.publicintelligence.net/USMC-CivilianCasualtiesMitigation.pdf](https://info.publicintelligence.net/USMC-CivilianCasualtiesMitigation.pdf) 

**'Civilian' is not defined in the Geneva conventions, what does the US define 'civilian' as?**
The DoD Law of War Manual and FM 6-27 contain the most current position of the Department of Defense as a whole on the law of war, which states:
[https://armypubs.army.mil/epubs/DR_pubs/DR_a/pdf/web/ARN19354_FM%206-27%20_C1_FINAL_WEB_v2.pdf](https://armypubs.army.mil/epubs/DR_pubs/DR_a/pdf/web/ARN19354_FM%206-27%20_C1_FINAL_WEB_v2.pdf)

**Who can be legally targeted?** In the context of terrorism, al-Qa’ida, the Taliban and its associated forces. In order to be deemed a "associated force" of al-Qa'ida or the Taliban, the entity must meet with two requirements. First, the organization must be an organized, militant group that has entered into a conflict alongside al-Qaeda or the Taliban. Second, the organization must be a co-belligerent with al-Qaeda or the Taliban in warfare against the United States or its coalition allies. Thus, a group is not an allied entity merely because it aligns with, or supports, al-Qa'ida or the Taliban. Merely engaged in acts of violence or merely sympathizing with al-Qaeda or the Taliban is not enough to put a group into the reach of the 2001 AUMF. Rather, the organization may also have joined al-Qaeda or the Taliban's war against the United States or its coalition allies.
[https://obamawhitehouse.archives.gov/sites/whitehouse.gov/files/documents/Legal_Policy_Report.pdf](https://obamawhitehouse.archives.gov/sites/whitehouse.gov/files/documents/Legal_Policy_Report.pdf)

**Legal Justifications:**

*Domestic Law:*
2001 AUMF
-
*International law:*
U.N. Security Council Authorization
The Inherent Right of Individual and Collective Self-Defense
Consent to Use Force in an Otherwise Lawful Manner **March 28, 2021 1:39 PM**

> In general, a civilian is a member of the civilian population—that is an individual who is neither part of nor associated with an armed force or group, nor otherwise engaging in hostilities. For example, any person who belongs to any of the combatant categories referred to in GPW Article 4(A)(1), (2), and (3) (these categories are discussed in paragraphs 3-16 to 3-25), are not civilians.
> 

**Drone Blowback: Much Ado about Nothing?**

**Drone Blowback: Much Ado about Nothing?** - [https://www.lawfareblog.com/drone-blowback-much-ado-about-nothing](https://www.lawfareblog.com/drone-blowback-much-ado-about-nothing)

[http://classic.austlii.edu.au/au/journals/AUYrBkIntLaw/1999/9.html](http://classic.austlii.edu.au/au/journals/AUYrBkIntLaw/1999/9.html)

[https://journals.sagepub.com/doi/abs/10.1177/0263276411423027](https://journals.sagepub.com/doi/abs/10.1177/0263276411423027) - This paper debunks the notion that drone pilots are desensitized to warfare and they treat targeting as a video game rather than a conflict with life-and-death implications (playstation mentality.)

UAV operators have considerably greater and more intimate interaction with the battlefield and the destruction of war than conventional fighter pilots , due to the intensely close sight of the attack and the target, and the rapid transition to the battle damage assessment after the attack.

[https://hhi.harvard.edu/sites/default/files/publications/conduct_of_military_operations_in_urban_areas.pdf](https://hhi.harvard.edu/sites/default/files/publications/conduct_of_military_operations_in_urban_areas.pdf) - Urban Warfare

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%2011.png)

[https://link.springer.com/chapter/10.1007%2F978-90-6704-811-8_9](https://link.springer.com/chapter/10.1007%2F978-90-6704-811-8_9)

> Drones are, however, not a panacea. While reliable data is difficult to obtain, civilians have at times been wrongly identified as targetable insurgents or terrorists.
> 
> 
> **It is equally incontestable that many civilians have been killed incidentally during drone strikes. Tragic as such losses are, they do not necessarily render the attacks unlawful.**
> 

From a leaked doc:

 [https://goodtimesweb.org/overseas-war/2015/small-footprint-operations-may-2013.pdf](https://goodtimesweb.org/overseas-war/2015/small-footprint-operations-may-2013.pdf) 

![https://media.discordapp.net/attachments/790042120483635260/808018172061810698/720c74e401e1bf458773144671181a94.png?ex=65ad4c90&is=659ad790&hm=13d6bbfeb6bd36c87a2f779374ab2b593cc52eefada54f5194eb314ea965c23a&=&format=webp&quality=lossless&width=550&height=70](https://media.discordapp.net/attachments/790042120483635260/808018172061810698/720c74e401e1bf458773144671181a94.png?ex=65ad4c90&is=659ad790&hm=13d6bbfeb6bd36c87a2f779374ab2b593cc52eefada54f5194eb314ea965c23a&=&format=webp&quality=lossless&width=550&height=70)

### Anwar Al-Aulaqi

Contrary to what many folks say, the strike against Anwar al-Aulaqi WAS LEGAL. 

Resources: 

 DOJ's May 25, 2011. White Paper on Legality of a Lethal Operations by the CIA against a US Citizen - ([https://cryptome.org/2014/09/doj-2011-targeting-citizens-nyt-14-0909.pdf](https://cryptome.org/2014/09/doj-2011-targeting-citizens-nyt-14-0909.pdf)) 

 DOJ July 16, 2010, memorandum, Re: Applicability Of Federal Criminal Laws and the Constitution to Contemplated Lethal Operations Against Shaykh Anwar al-Aulaqi - ([https://fas.org/irp/agency/doj/olc/aulaqi.pdf](https://fas.org/irp/agency/doj/olc/aulaqi.pdf))

 DOJ White Paper, Lawfulness of a Lethal Operation Directed Against a U.S. Citizen Who Is a Senior Operational Leader Of Al-Qaida or An Associated Force - ([https://fas.org/irp/eprint/doj-lethal.pdf](https://fas.org/irp/eprint/doj-lethal.pdf))

[https://www.federalregister.gov/documents/2010/07/23/2010-18102/designation-of-anwar-al-aulaqi-pursuant-to-executive-order-13224-and-the-global-terrorism-sanctions](https://www.federalregister.gov/documents/2010/07/23/2010-18102/designation-of-anwar-al-aulaqi-pursuant-to-executive-order-13224-and-the-global-terrorism-sanctions)

[Designation of ANWAR AL-AULAQI Pursuant to Executive Order 13224 and the Global Terrorism Sanctions Regulations, 31 CFR Part 594](https://www.federalregister.gov/documents/2010/07/23/2010-18102/designation-of-anwar-al-aulaqi-pursuant-to-executive-order-13224-and-the-global-terrorism-sanctions)

### Resources Regarding the Soleimani Strike

[https://assets.documentcloud.org/documents/6808252/DOD-GC-Speech-BYU-QS.pdf](https://assets.documentcloud.org/documents/6808252/DOD-GC-Speech-BYU-QS.pdf)

Legal:

[https://crsreports.congress.gov/product/pdf/R/R46148](https://crsreports.congress.gov/product/pdf/R/R46148)

Q&A:

[https://www.lawfareblog.com/was-soleimani-killing-assassination](https://www.lawfareblog.com/was-soleimani-killing-assassination)

[https://www.treasury.gov/press-center/press-releases/Pages/hp644.aspx](https://www.treasury.gov/press-center/press-releases/Pages/hp644.aspx)

**[Redacted OLC Memo Justification of Soleimani Strike](https://www.documentcloud.org/documents/21012045-redacted-olc-memo-justification-of-soleimani-strike)**

### Critiques of an ICRC Study on Dronestrikes

These several papers critique a flawed ICRC study:

1. ***Oppor­tu­ni­ty Lost: Orga­nized Armed Groups and the ICRC “Direct Par­tic­i­pa­tion in Hos­til­i­ties” Inter­pre­tive Guid­ance***
    
    [https://web.archive.org/web/20170617213851/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Watkin.pdf](https://web.archive.org/web/20170617213851/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Watkin.pdf)
    

1. ***Decon­struct­ing Direct Par­tic­i­pa­tion in Hos­til­i­ties: The Con­sti­tu­tive Ele­ments***
    
    [https://web.archive.org/web/20190830170604/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Schmitt.pdf](https://web.archive.org/web/20190830170604/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Schmitt.pdf)
    

1. ***And for Such Time As”: The Time Dimen­sion to Direct Par­tic­i­pa­tion in Hos­til­i­ties***
    
    [https://web.archive.org/web/20170617214238/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Boothby.pdf](https://web.archive.org/web/20170617214238/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Boothby.pdf)
    

1. ***Part IX of the ICRC “Direct Par­tic­i­pa­tion in Hos­til­i­ties” Study: No Man­date, No Exper­tise, and Legal­ly Incor­rect***
    
    [https://web.archive.org/web/20170618044614/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Parks.pdf](https://web.archive.org/web/20170618044614/http://nyujilp.org/wp-content/uploads/2012/04/42.3-Parks.pdf)
    

1. ***Civilians with skin in the game: The Law of War Manual's rejection of the ICRC guidance on Direct Participation in Hostilities***
    
    [https://www.loc.gov/rr/frd/Military_Law/Military_Law_Review/pdf-files/225-issue2-2017.pdf](https://www.loc.gov/rr/frd/Military_Law/Military_Law_Review/pdf-files/225-issue2-2017.pdf)
    

[https://web.archive.org/web/20090901204805/https://www.hks.harvard.edu/cchrp/Use%20of%20Force/October%202002/Parks_final.pdf](https://web.archive.org/web/20090901204805/https://www.hks.harvard.edu/cchrp/Use%20of%20Force/October%202002/Parks_final.pdf)

^ This memo concludes that the use of military force against legitimate targets in times of war, or against similar targets in times of peace, where such persons or groups pose an immediate threat to US people or national security, as established by relevant authority, does not constitute assassination or plot to commit assassination.

### From *The Intercept*'s *Drone Papers*

Confirms the time, care and effort into HVT operations planning and PID. 
At the bottom, it shows only after host-nation approval can a strike go ahead. 

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%2012.png)

**Report to the Director of National Intelligence on the Fort Hood and Northwest Flight 253 Incidents**

p.25

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%2013.png)

![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%2014.png)

## Also Posted under *International Law*

[U. S. Military Operations Law, Policy, And Practice OUP '15 : Free Download, Borrow, and Streaming : Internet Archive](https://archive.org/details/geoffrey-s.-corn-rachel-e.-van-landingham-shane-r.-reeves-eds.-u.-s.-military-op_202102/mode/2up)

[Handbook Of The International Law Of Military Operations OUP '15 : Free Download, Borrow, and Streaming : Internet Archive](https://archive.org/details/fleck-dieter-gill-terry-d-the-handbook-of-the-international-law-of-military-oper/mode/2up)

[Practitioners Guide To Human Rights Law In Armed Conflict : Free Download, Borrow, and Streaming : Internet Archive](https://archive.org/details/daragh-murray-elizabeth-wilmshurst-francoise-hampson-charles-garraway-noam-lubel/mode/2up)


## Various Resources

1. ***Deadly Advice: Judge Advocates and Joint Targeting***
    
    Fantastic short paper about the role of Military Lawyers in their role in targeting
    
    [https://archive.org/details/06201611/mode/2up](https://archive.org/details/06201611/mode/2up)
    
2. ***Targeted Killing and Accountability***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1819583](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1819583)
    "This Article is a necessary corrective to the public and scholarly debate. It has presented the complex web of bureaucratic, legal, professional, and political accountability mechanisms that exert influence over the targeted killing process.It has demonstrated that many of the critiques of targeted killings rest upon poorly conceived understandings of the process, unclear definitions, and unsubstantiated speculation." 
    
3. ***Rules of Engagement Demystified: A Study of the History, Development and Use of ROEs***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2602763](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2602763)
    
4. ***The French Military Intervention in Mali, Counter-Terrorism, and the Law of Armed Conflict***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2604235](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2604235)
    
5. ***False Rubicons, Moral Panic & Conceptual Cul-De-Sacs: Critiquing & Reframing the Call to Ban Lethal Autonomous Weapons***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2736407](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2736407)
    
6. ***Targeting and Civilian Risk Mitigation: The Essential Role of Precautionary Measures***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2889472](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2889472)
    
7. ***Two Sides of the Combatant COIN: Untangling Direct Participation in Hostilites from Belligerent Status in Non-International Armed Conflicts***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1604626](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1604626)
    
8. ***Targeting and International Humanitarian Law in Afghanistan***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1600272](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1600272)
    
9. ***It is Not Self-Defense: Direct Participation in Hostilities Authority at the Tactical Level***
    
    [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2984267](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2984267)
    "A third basis for use of force exists specifically to address situations such as the IED emplacer—the authority to attack any person who is directly participating in hostilities—referred to as direct participation authority.23 Civilians who elect to take direct part in hostilities may be attacked for such time as they directly participate in hostilities.24 The authority to attack civilians who take a direct part in hostilities is an exception to the general rule that civilians may not be attacked.25 This authority is recognized in international law by the Geneva Conventions,26 the Additional Protocols to the Geneva Conventions,27 the International Committee of the Red Cross,28 and a long history of customary practice.29 Additionally, it is explicitly recognized by the United States in the new U.S. Department of Defense Law of War Manual.”
    
10. ***Forged in the Fire - Legal Lessons Learned During Military Operations***
    
    [https://www.loc.gov/rr/frd/Military_Law/pdf/forged-in-the-fire-2008.pdf](https://www.loc.gov/rr/frd/Military_Law/pdf/forged-in-the-fire-2008.pdf)
    
11. This memo concludes that the use of military force against legitimate targets in times of war, or against similar targets in times of peace, where such persons or groups pose an immediate threat to US people or national security, as established by relevant authority, does not constitute assassination or plot to commit assassination.
    
    [https://web.archive.org/web/20090901204805/https://www.hks.harvard.edu/cchrp/Use%20of%20Force/October%202002/Parks_final.pdf](https://web.archive.org/web/20090901204805/https://www.hks.harvard.edu/cchrp/Use%20of%20Force/October%202002/Parks_final.pdf)
    
12. ***Development of a DOD Instruction on Minimizing and Responding to Civilian Harm in Military Operations***
    
    [https://media.defense.gov/2020/Feb/20/2002252367/-1/-1/1/DEVELOPMENT-OF-A-DOD-INSTRUCTION-ON-MINIMIZING-AND-RESPONDING-TO-CIVILIAN-HARM-IN-MILITARY-OPERATIONS.PDF](https://media.defense.gov/2020/Feb/20/2002252367/-1/-1/1/DEVELOPMENT-OF-A-DOD-INSTRUCTION-ON-MINIMIZING-AND-RESPONDING-TO-CIVILIAN-HARM-IN-MILITARY-OPERATIONS.PDF)
    

1. Trump replacement (PSP) to the 2013 PPG, made public by the ACLU via litigation:
    
    [https://www.justice.gov/oip/foia-library/procedures_for_approving_direct_action_against_terrorist_targets/download](https://www.justice.gov/oip/foia-library/procedures_for_approving_direct_action_against_terrorist_targets/download)
    
    [https://www.aclu.org/sites/default/files/field_document/2021-4-30_psp_foia_final.pdf](https://www.aclu.org/sites/default/files/field_document/2021-4-30_psp_foia_final.pdf)
    
    The PSP commits to adherence to domestic/international law and CIVCAS mitigation efforts.
    Some sections of the 2017 PSP are reproduced below:
    
    ![Untitled](US%20Wars%20&%20Conflicts%2037350716c2ac49da9daab07cce43d974/Untitled%2015.png)
    
2. **[Knock-on-the-Roof: The U.S. Air Force’s New Tactic](https://warontherocks.com/2016/05/knock-on-the-roof-the-u-s-air-forces-new-tactic/)**
[https://warontherocks.com/2016/05/knock-on-the-roof-the-u-s-air-forces-new-tactic/](https://warontherocks.com/2016/05/knock-on-the-roof-the-u-s-air-forces-new-tactic/)
3. ***Legitmate Military Objective Under The Current Jus in Bello***
    
    [https://digital-commons.usnwc.edu/cgi/viewcontent.cgi?article=1361&context=ils](https://digital-commons.usnwc.edu/cgi/viewcontent.cgi?article=1361&context=ils)
    
4. ***The Non-combatant Casualty Cut-off Value: Assessment of a Novel Targeting Technique in Operation Inherent Resolve***
    
    [https://archive.org/details/international-criminal-law-review-volume-issue-2018-doi-10.1163-15718123-01804002-graham-scott-th](https://archive.org/details/international-criminal-law-review-volume-issue-2018-doi-10.1163-15718123-01804002-graham-scott-th)
    
    "Media and human rights groups’ criticism that coalition reliance on the NCV constitutes a war crime finds no support upon reading the Rome Statute. To the contrary, policy-based, command-driven, plans-led, operational-execution of the NCV is a sound legal and strategic measure to defeat isis whilst minimizing civilian casualties. Beneficially, NCV restrictions compel commanders to conduct focused proportionality assessments to respond to prevailing operational threats."
    
5. **[*Handbook Of The International Law Of Military Operations OUP '15*](https://archive.org/details/fleck-dieter-gill-terry-d-the-handbook-of-the-international-law-of-military-oper/mode/2up)**
    
    [https://archive.org/details/fleck-dieter-gill-terry-d-the-handbook-of-the-international-law-of-military-oper/mode/2up](https://archive.org/details/fleck-dieter-gill-terry-d-the-handbook-of-the-international-law-of-military-oper/mode/2up)
    
    Shows that the US, as a matter of policy, has always been committed to upholding and enhancing compliance to their LOAC obligations.
    
    > My Lai changed substantially the traditional role of military lawyers in the planning and execution phases of military operations. And it professionalized a new area of law—Operational Law—thus fostering its further development.
    > 
6. ***Case Studies in Strategic Bombardment***
    
    [https://media.defense.gov/2010/Oct/12/2001330115/-1/-1/0/AFD-101012-036.pdf](https://media.defense.gov/2010/Oct/12/2001330115/-1/-1/0/AFD-101012-036.pdf)
    
7. German Court ruling upholding legality of drones (English translation) [https://usnwc.libguides.com/ld.php?content_id=20600363](https://usnwc.libguides.com/ld.php?content_id=20600363)
8. ***The Interpretive Guidance on the Notion of Direct Participation in Hostilities: A Critical Analysis***
    
    [https://harvardnsj.org/wp-content/uploads/sites/13/2015/01/Vol.-1_Schmitt_Final.pdf](https://harvardnsj.org/wp-content/uploads/sites/13/2015/01/Vol.-1_Schmitt_Final.pdf)
    
9.  ***Special Forces' Wear of Non-Standard Uniforms*** [https://chicagounbound.uchicago.edu/cgi/viewcontent.cgi?article=1225&context=cjil](https://chicagounbound.uchicago.edu/cgi/viewcontent.cgi?article=1225&context=cjil)
10. May 2021 IG report (released Dec 22)
    
    [https://media.defense.gov/2021/Dec/10/2002907413/-1/-1/1/DODIG-2021-084%20(REDACTED).PDF](https://media.defense.gov/2021/Dec/10/2002907413/-1/-1/1/DODIG-2021-084%20(REDACTED).PDF)
    Confirms CENTCOM adheres to pre-strike approval, CIVCAS mitigation, etc. procedures.
    
11. ***Were Drone Strikes Effective? Evaluating the Drone Campaign in Pakistan Through Captured al-Qaeda Documents***
    
    [https://inss.ndu.edu/Media/News/Article/2976119/were-drone-strikes-effective-evaluating-the-drone-campaign-in-pakistan-through/](https://inss.ndu.edu/Media/News/Article/2976119/were-drone-strikes-effective-evaluating-the-drone-campaign-in-pakistan-through/)
    
    "This systematic review of declassified Arabic-language correspondence among senior al-Qaeda leaders and operatives suggests that drone strikes eroded the quality of al-Qaeda’s personnel base, forced the group to reduce communications and other activities, and compelled it to flee its safe haven in Pakistan’s tribal regions."
    
12. ***PRECISE AND POPULAR: WHY PEOPLE IN NORTHWEST PAKISTAN SUPPORT DRONES***
    
    [https://warontherocks.com/2022/08/precise-and-popular-why-people-in-northwest-pakistan-support-drones/](https://warontherocks.com/2022/08/precise-and-popular-why-people-in-northwest-pakistan-support-drones/)