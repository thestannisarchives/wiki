# Afghanistan

## Background

**Afghanistan: Background and U.S. Policy: In Brief**
[https://fas.org/sgp/crs/row/R45122.pdf](https://fas.org/sgp/crs/row/R45122.pdf "https://fas.org/sgp/crs/row/R45122.pdf")

**The war in Afghanistan : a legal analysis**
[https://archive.org/details/warinafghanistan85schm](https://archive.org/details/warinafghanistan85schm "https://archive.org/details/warinafghanistan85schm")
This treatise, part of the US Naval War College's International Law Studies blue book series, is a collection of scholarly publications that resulted from a series of workshops hosted by the College. Covers legal basis for US operations, non-international armed conflict, detention operations, targeting, etc.

**Details of CIA activities in 2001-2002**
[https://www.archives.gov/files/declassification/iscap/pdf/2012-041-doc01.pdf](https://www.archives.gov/files/declassification/iscap/pdf/2012-041-doc01.pdf "https://www.archives.gov/files/declassification/iscap/pdf/2012-041-doc01.pdf")

**Investing in the Fight: Assessing the Use of the Commander's Emergency Response Program in Afghanistan**
[https://www.rand.org/pubs/research_reports/RR1508.html](https://www.rand.org/pubs/research_reports/RR1508.html "https://www.rand.org/pubs/research_reports/RR1508.html")
Finds that US commanders preferred to use CERP instead of kinetic strikes. Highly effective.

 **Assessment of conditions of women in Afghanistan**
[https://www.dni.gov/files/ODNI/documents/assessments/SOCM-AFG_Women.pdf](https://www.dni.gov/files/ODNI/documents/assessments/SOCM-AFG_Women.pdf "https://www.dni.gov/files/ODNI/documents/assessments/SOCM-AFG_Women.pdf")  ^   

> Afghanistan’s progress since the end of Taliban rule toward meeting
> broadly accepted international standards for the conditions of women
> has been uneven, reflecting cultural norms and conflict. The Taliban
> regime barred girls from attending school and prohibited women from
> working outside the home or being in public without a male relative.
> Although the Taliban’s fall officially ended some policies, many
> continue in practice even in government-controlled areas, and years of
> war have left millions of women maimed, widowed, impoverished, and
> displaced.

**Who is responsible for the Taliban?**
https://www.washingtoninstitute.org/policy-analysis/who-responsible-taliban

**Did the U.S. "Create" Osama bin Laden?**
_Allegations that the U.S. provided funding for bin Laden proved inaccurate_  [https://web.archive.org/web/20050310111109/http://usinfo.state.gov/media/Archive/2005/Jan/24-318760.html](https://web.archive.org/web/20050310111109/http://usinfo.state.gov/media/Archive/2005/Jan/24-318760.html "https://web.archive.org/web/20050310111109/http://usinfo.state.gov/media/Archive/2005/Jan/24-318760.html")
 [https://web.archive.org/web/20200815061918if_/https://www.cia.gov/news-information/cia-the-war-on-terrorism/terrorism-faqs.html?tab=list-1](https://web.archive.org/web/20200815061918if_/https://www.cia.gov/news-information/cia-the-war-on-terrorism/terrorism-faqs.html?tab=list-1 "https://web.archive.org/web/20200815061918if_/https://www.cia.gov/news-information/cia-the-war-on-terrorism/terrorism-faqs.html?tab=list-1")  
 "No. Numerous comments in the media recently have reiterated a widely circulated but incorrect notion that the CIA once had a relationship with Usama Bin Laden. For the record, you should know that the CIA never employed, paid, or maintained any relationship whatsoever with Bin Laden."

## Soviet-Afghan War

**A brief assessment of the Soviet invasion of afghanistan**
[https://www.cia.gov/readingroom/docs/DOC_0000278538.pdf](https://www.cia.gov/readingroom/docs/DOC_0000278538.pdf "https://www.cia.gov/readingroom/docs/DOC_0000278538.pdf")

**Translated Soviet study on tactical lessons learned from Afghanistan**
[https://apps.dtic.mil/dtic/tr/fulltext/u2/a316729.pdf](https://apps.dtic.mil/dtic/tr/fulltext/u2/a316729.pdf "https://apps.dtic.mil/dtic/tr/fulltext/u2/a316729.pdf")
Submitted by @Randy

**Afghanistan Situation Report: June 1985**
[https://documents2.theblackvault.com/documents/cia/AfghanistanSituationReport-4June1985.pdf](https://documents2.theblackvault.com/documents/cia/AfghanistanSituationReport-4June1985.pdf "https://documents2.theblackvault.com/documents/cia/AfghanistanSituationReport-4June1985.pdf")

***Afghanistan: Prospects For The Resistance***
[https://www.cia.gov/readingroom/docs/DOC_0005564710.pdf](https://www.cia.gov/readingroom/docs/DOC_0005564710.pdf "https://www.cia.gov/readingroom/docs/DOC_0005564710.pdf")
Discusses the environment in Afghanistan 4yrs after the Soviet invasion.
![Image](https://images-ext-2.discordapp.net/external/sWEkuNe8Amjf8vZlAm-0kQfcr_V8-AcYFVhSjE8SLsI/https/i.imgur.com/qe7kjqQ.png?format=webp&quality=lossless)
^ Highlights the goals of the imperialist Soviet Union.

 Great intro on Soviet Afghan war, especially on precursors to the invasion: [https://up.raindrop.io/raindrop/files/268/461/707/Anthony_Hyman_auth_Afghanistan_under_Soviet_Domination_1964_91_Palgrave_Macmillan_UK_1992.pdf](https://up.raindrop.io/raindrop/files/268/461/707/Anthony_Hyman_auth_Afghanistan_under_Soviet_Domination_1964_91_Palgrave_Macmillan_UK_1992.pdf "https://up.raindrop.io/raindrop/files/268/461/707/Anthony_Hyman_auth_Afghanistan_under_Soviet_Domination_1964_91_Palgrave_Macmillan_UK_1992.pdf") Thorough detailing of early efforts to resist the DRA: [https://up.raindrop.io/raindrop/files/268/465/047/Mohammed_Kakar_Afghanistan_The_Soviet_Invasion_and_the_Afghan_Response_1979_1982_University_of_Calif](https://up.raindrop.io/raindrop/files/268/465/047/Mohammed_Kakar_Afghanistan_The_Soviet_Invasion_and_the_Afghan_Response_1979_1982_University_of_Calif "https://up.raindrop.io/raindrop/files/268/465/047/Mohammed_Kakar_Afghanistan_The_Soviet_Invasion_and_the_Afghan_Response_1979_1982_University_of_Calif") (edited)
 Submitted by @Randy

**Milestones: 1977–1980 - Office of the Historian**
[https://history.state.gov/milestones/1977-1980/soviet-invasion-afghanistan](https://history.state.gov/milestones/1977-1980/soviet-invasion-afghanistan "https://history.state.gov/milestones/1977-1980/soviet-invasion-afghanistan")
>In sum, these actions were Washington’s collective attempt to make the Soviets’ “adventure” in Afghanistan as painful and brief as possible. Instead, it took ten years of grinding insurgency before Moscow finally withdrew, at the cost of millions of lives and billions of dollars. In their wake, the Soviets left a shattered country in which the Taliban, an Islamic fundamentalist group, seized control, later providing Osama bin Laden with a training base from which to launch terrorist operations worldwide.
 
## Operation Enduring Freedom

_**The United States Army in Afghanistan - Operation Enduring Freedom, 2005–2009**_
[https://history.army.mil/html/books/070/70-131/cmhPub_70-131-1.pdf](https://history.army.mil/html/books/070/70-131/cmhPub_70-131-1.pdf "https://history.army.mil/html/books/070/70-131/cmhPub_70-131-1.pdf")

_**A Good Ally: Norway in Afghanistan 2001–2014**_
[https://www.regjeringen.no/contentassets/09faceca099c4b8bac85ca8495e12d2d/en-gb/pdfs/nou201620160008000engpdfs.pdf](https://www.regjeringen.no/contentassets/09faceca099c4b8bac85ca8495e12d2d/en-gb/pdfs/nou201620160008000engpdfs.pdf "https://www.regjeringen.no/contentassets/09faceca099c4b8bac85ca8495e12d2d/en-gb/pdfs/nou201620160008000engpdfs.pdf")
Norwegian Commission on Afghanistan. A strong report on at least their side of the conflict.

**Appropriations for Afghanistan Reconstruction**
US appropriated around $114 billion to Afghanistan reconstruction since 2001.
[https://www.sigar.mil/pdf/lessonslearned/SIGAR-21-41-LL.pdf](https://www.sigar.mil/pdf/lessonslearned/SIGAR-21-41-LL.pdf "https://www.sigar.mil/pdf/lessonslearned/SIGAR-21-41-LL.pdf")


![Image](https://media.discordapp.net/attachments/811937945732775966/875882455662477322/ELYl6XVXkAA_CP_.png?ex=65b43f1a&is=65a1ca1a&hm=4227e2171ac483a43c1c4278e0c73a2ac2bdd286e67dab6123b78959da24d15f&=&format=webp&quality=lossless)
Just two examples of these new Afghan FOIA docs from SIGAR - the cherry-picking from WaPo is some of the worst I've seen in a long time. No lies found, no misrepresentations - & success stories totally ignored. SIGAR critiques have been well known for years.


**Congressional Report on human rights abuses by Afghan security forces**
[https://www.sigar.mil/pdf/inspections/SIGAR%2017-47-IP.pdfA](https://www.sigar.mil/pdf/inspections/SIGAR%2017-47-IP.pdfA "https://www.sigar.mil/pdf/inspections/SIGAR%2017-47-IP.pdfA")
2017 report, requested by Congress, examined between FY 2011–16, finds that the US investigated all allegations of human rights abuses committed by Afghan forces, including child abuse reports made externally and internally, as best they could and cut off aid to those units implicated. Convictions were landed on those who committed the crimes. The report does find some issues, and the agencies involved have their comments at the end. There wasn't a consolidated process, and as US forces drew down—they lacked the insights they once had into abuse allegations.  "Following the establishment of the State and Defense Leahy Vetting Procedures for the Afghan National Security Forces in July 2014, and a biweekly joint DOD and State Afghanistan Gross Violation of Human Rights Forum (Leahy Forum), the Office of the Under Secretary of Defense for Policy (OUSD-P) began to track gross violation of human rights incidents—including child sexual assault—that were reported to and considered by the Leahy Forum. According to data provided by OUSD-P, as of August 12, 2016, the office was tracking 75 reported gross violation of human rights incidents. Of these reported incidents, 7 involved child sexual assault, 46 involved other gross violations of human rights, and 22 were classified at a level above Secret because of the sensitivity of the information or the sources and methods used to obtain the information. These incidents ranged in date from 2010 through 2016, and included gross violations of human rights allegedly committed by Afghan security forces within the MOD and MOI. The incidents reported to and considered by the Leahy Forum came from a variety of sources including intelligence reports, news articles, U.S. forces, and the Afghan government."  "Of the 75 reported incidents in the OUSD-P tracker, 7 involved allegations of child sexual assault—1 that the Leahy Forum found to be credible, 5 that remain under review, and 1 that was found not credible. Fiveof the seven involved MOI units, and two involved MOD units. The one credible incident was reported by the Afghan government. In this instance, the government reported that two ANA Special Operations Command (ANASOC) noncommissioned officers (NCOs) attempted to sexually assault a girl to coerce information from hermother. Following a trial, both NCOs were indicted and convicted for attempted sexual assault of a minor in violation of the penal code and sentenced to 6 years of confinement. Based on the information the Afghan government provided, DOD determined that it had credible information of a gross violation of human rights and began the process to remediate the unit." Also details improvements made on the Afghan side including legislative, as well as other high-level efforts by the US Government working with the Afghans. It found no evidence, as NGOs/media alleged, that US forces were told to ignore child sex abuse/other human rights abuses.
![Image](https://media.discordapp.net/attachments/811937945732775966/851981758270537748/4a284473bb3f8c9c01115a7250a96e37.png?ex=65b05a58&is=659de558&hm=2f9e8337fcc42c4c90d61ea7130af8844c1429656f472b4a63149a23dae612d2&=&format=webp&quality=lossless)

[https://www.cia.gov/static/9fa9520134e1c2f3068001de69c9964b/Curator-Pocket-History-CIA.pdf](https://www.cia.gov/static/9fa9520134e1c2f3068001de69c9964b/Curator-Pocket-History-CIA.pdf "https://www.cia.gov/static/9fa9520134e1c2f3068001de69c9964b/Curator-Pocket-History-CIA.pdf")  "Fifteen days after 9/11, a handful of CIA paramilitary officers landed in Afghanistan and reestablished relationships with local fighters who had received CIA aid in fighting Soviet occupiers during the 1980s."

[https://www.defense.gov/Newsroom/Transcripts/Transcript/Article/2582980/general-kenneth-f-mckenzie-jr-commander-us-central-command-holds-a-press-briefi/](https://www.defense.gov/Newsroom/Transcripts/Transcript/Article/2582980/general-kenneth-f-mckenzie-jr-commander-us-central-command-holds-a-press-briefi/ "https://www.defense.gov/Newsroom/Transcripts/Transcript/Article/2582980/general-kenneth-f-mckenzie-jr-commander-us-central-command-holds-a-press-briefi/")
![Image](https://media.discordapp.net/attachments/811937945732775966/877457458736533554/EzyPZfLVUAIvQ6U.png?ex=65b0bf70&is=659e4a70&hm=151f874d1785b6eb43965c944f4655a8e66ba522ebd3c875eb99b40e23d2ba89&=&format=webp&quality=lossless)

**CERP Projects in Afghanistan Proved Effective**
[http://www.rand.org/pubs/research_reports/RR1508.html](http://www.rand.org/pubs/research_reports/RR1508.html "http://www.rand.org/pubs/research_reports/RR1508.html")

## Opium

***Counternarcotics: Lessons from the U.S. Experience in Afghanistan*** https://www.sigar.mil/interactive-reports/counternarcotics/index.html
Goes over the length of US' counter-narcotics efforts. Counter to conspiracy theories, the US didn't allow them to grow and protect the fields. They were actively destroying them, to varying degrees of success

_**Fighting the Opium Trade in Afghanistan: Myths, Facts, and Sound Policy**_
[https://2001-2009.state.gov/p/inl/rls/other/102214.htm](https://2001-2009.state.gov/p/inl/rls/other/102214.htm "https://2001-2009.state.gov/p/inl/rls/other/102214.htm")

**Fact Brief: Do US pharmaceutical companies source opium from Afghanistan?**
[https://repustar.com/fact-briefs/do-us-pharmaceutical-companies-source-opium-afghanistan](https://repustar.com/fact-briefs/do-us-pharmaceutical-companies-source-opium-afghanistan "https://repustar.com/fact-briefs/do-us-pharmaceutical-companies-source-opium-afghanistan")

Under Taliban rule, drug cultivation skyrockets, according to classified documents: [https://nsarchive.gwu.edu/sites/default/files/2023-01/NIE%20May%202001%20report.pdf#page=26](https://nsarchive.gwu.edu/sites/default/files/2023-01/NIE%20May%202001%20report.pdf#page=26 "https://nsarchive.gwu.edu/sites/default/files/2023-01/NIE%20May%202001%20report.pdf#page=26")  

[https://nsarchive.gwu.edu/sites/default/files/2023-01/CIA%20Dec.%201998%20report.pdf](https://nsarchive.gwu.edu/sites/default/files/2023-01/CIA%20Dec.%201998%20report.pdf "https://nsarchive.gwu.edu/sites/default/files/2023-01/CIA%20Dec.%201998%20report.pdf")  

[https://web.archive.org/web/20220628153722/https://www.ojp.gov/pdffiles1/Digitization/151459NCJRS.pdf](https://web.archive.org/web/20220628153722/https://www.ojp.gov/pdffiles1/Digitization/151459NCJRS.pdf "https://web.archive.org/web/20220628153722/https://www.ojp.gov/pdffiles1/Digitization/151459NCJRS.pdf")

## Withdrawal

**Not An Intelligence Failure — Something Much Worse**
[https://www.justsecurity.org/77801/cias-former-counterterrorism-chief-for-the-region-afghanistan-not-an-intelligence-failure-something-much-worse/](https://www.justsecurity.org/77801/cias-former-counterterrorism-chief-for-the-region-afghanistan-not-an-intelligence-failure-something-much-worse/ "https://www.justsecurity.org/77801/cias-former-counterterrorism-chief-for-the-region-afghanistan-not-an-intelligence-failure-something-much-worse/")
https://web.archive.org/web/20240114025844/https://www.justsecurity.org/77801/cias-former-counterterrorism-chief-for-the-region-afghanistan-not-an-intelligence-failure-something-much-worse/
>The U.S. Intelligence Community assessed Afghanistan’s fortunes according to various scenarios and conditions and depending on the multiple policy alternatives from which the president could choose. So, was it 30 days from withdrawal to collapse? 60? 18 months? Actually, it was all of the above, the projections aligning with the various “what ifs.” Ultimately, it was assessed, Afghan forces might capitulate **within days** under the circumstances we witnessed, in projections highlighted to Trump officials and future Biden officials alike.

**WSJ Opinion: U.S. Spies Didn’t Cause Kabul to Fall**
https://www.wsj.com/amp/articles/spies-cia-kabul-fall-afghanistan-withdrawal-crisis-collapse-taliban-intelligence-failure-11630509018?mod=hp_opin_pos_4

**Afghanistan Study Group Final Report**
[https://www.usip.org/publications/2021/02/afghanistan-study-group-final-report-pathway-peace-afghanistan](https://www.usip.org/publications/2021/02/afghanistan-study-group-final-report-pathway-peace-afghanistan "https://www.usip.org/publications/2021/02/afghanistan-study-group-final-report-pathway-peace-afghanistan")

**Inside the State Department’s Afghanistan Evacuation**
[https://www.thedailybeast.com/the-real-story-of-the-state-departments-afghanistan-evacuation](https://www.thedailybeast.com/the-real-story-of-the-state-departments-afghanistan-evacuation "https://www.thedailybeast.com/the-real-story-of-the-state-departments-afghanistan-evacuation")

**America Needs Accountability on Afghanistan**
[https://www.thecipherbrief.com/column_article/america-needs-accountability-on-afghanistan](https://www.thecipherbrief.com/column_article/america-needs-accountability-on-afghanistan "https://www.thecipherbrief.com/column_article/america-needs-accountability-on-afghanistan")
![Image](https://media.discordapp.net/attachments/696601380026318862/1195929753694257242/image.png?ex=65b5c760&is=65a35260&hm=85443acbee3dc106f32ec5da7a3e0e6bedadcd44b2392619e1d6df37704b6ce5&=&format=webp&quality=lossless)

***Modern War in an Ancient Land*  Vol 1 & 2** 
[https://history.army.mil/html/books/059/59-1/](https://history.army.mil/html/books/059/59-1/ "https://history.army.mil/html/books/059/59-1/") 
Just published nov 2021.

[https://www.sigar.mil/pdf/evaluations/SIGAR-22-22-IP.pdf](https://www.sigar.mil/pdf/evaluations/SIGAR-22-22-IP.pdf "https://www.sigar.mil/pdf/evaluations/SIGAR-22-22-IP.pdf")  
Pins blame squarely on the Doha agreement for the failing of the Afghanistan Government. No surprise there.
![Image](https://media.discordapp.net/attachments/696601380026318862/1195930722784006184/image.png?ex=65b5c847&is=65a35347&hm=ede04c74c535b01094465712594dd7315b71fa0288377b7d729894ea985016c5&=&format=webp&quality=lossless)

![Image](https://media.discordapp.net/attachments/696601380026318862/1195931988268429463/image.png?ex=65b5c975&is=65a35475&hm=89b98275bbc1afe25198bb7cd4fd49d55178246cb9442eedd7182f4051a67e9a&=&format=webp&quality=lossless)

SecDef Austin statement re: Defense Department's Afghanistan after action review:
[https://www.defense.gov/News/Releases/Release/Article/3354951/statement-by-secretary-of-defense-lloyd-j-austin-iii-on-the-defense-departments/](https://www.defense.gov/News/Releases/Release/Article/3354951/statement-by-secretary-of-defense-lloyd-j-austin-iii-on-the-defense-departments/ "https://www.defense.gov/News/Releases/Release/Article/3354951/statement-by-secretary-of-defense-lloyd-j-austin-iii-on-the-defense-departments/")  [https://www.whitehouse.gov/wp-content/uploads/2023/04/US-Withdrawal-from-Afghanistan.pdf](https://www.whitehouse.gov/wp-content/uploads/2023/04/US-Withdrawal-from-Afghanistan.pdf "https://www.whitehouse.gov/wp-content/uploads/2023/04/US-Withdrawal-from-Afghanistan.pdf")

## Various Resources

1. **Afghanistan: Post-Taliban Governance, Security, and U.S. Policy**
		[https://www.everycrsreport.com/reports/RL30588.html](https://www.everycrsreport.com/reports/RL30588.html "https://www.everycrsreport.com/reports/RL30588.html") 
		Provides a detailed overview; covers post-Taliban governance, security, history, and US policy of Afghanistan, dispels many myths
2. _**Detainee Review Boards in Afghanistan: From Strategic Liability to Legitimacy**_
	[https://www.loc.gov/rr/frd/Military_Law/pdf/Bovarnick-Detainee.pdf](https://www.loc.gov/rr/frd/Military_Law/pdf/Bovarnick-Detainee.pdf "https://www.loc.gov/rr/frd/Military_Law/pdf/Bovarnick-Detainee.pdf")
3. [U.S. Army Cultural Support Team Program: Historical Timeline](https://arsof-history.org/articles/v12n2_cst_timeline_page_1.html)
4. [https://www.cia.gov/static/9fa9520134e1c2f3068001de69c9964b/Curator-Pocket-History-CIA.pdf](https://www.cia.gov/static/9fa9520134e1c2f3068001de69c9964b/Curator-Pocket-History-CIA.pdf "https://www.cia.gov/static/9fa9520134e1c2f3068001de69c9964b/Curator-Pocket-History-CIA.pdf")  "Fifteen days after 9/11, a handful of CIA paramilitary officers landed in Afghanistan and reestablished relationships with local fighters who had received CIA aid in fighting Soviet occupiers during the 1980s."
5. ***Afghanistan Camps Central to 11 September Plot: Can Al-Qa'ida Train on the Run?***  [https://nsarchive.gwu.edu/sites/default/files/documents/qx7xqj-swjpt/2003-06-20-afghanistan-camps-central-to-11.pdf](https://nsarchive.gwu.edu/sites/default/files/documents/qx7xqj-swjpt/2003-06-20-afghanistan-camps-central-to-11.pdf "https://nsarchive.gwu.edu/sites/default/files/documents/qx7xqj-swjpt/2003-06-20-afghanistan-camps-central-to-11.pdf")
6. Afghan history brief by DIA: [https://info.publicintelligence.net/DIA-AfghanHistory.pdf](https://info.publicintelligence.net/DIA-AfghanHistory.pdf "https://info.publicintelligence.net/DIA-AfghanHistory.pdf")
7. **What We Need to Learn: Lessons from Twenty Years of Afghanistan**
	[https://www.sigar.mil/interactive-reports/what-we-need-to-learn/index.html](https://www.sigar.mil/interactive-reports/what-we-need-to-learn/index.html "https://www.sigar.mil/interactive-reports/what-we-need-to-learn/index.html")
8. Support for Gender Equality: Lessons from the U.S. Experience in Afghanistan
	[https://www.sigar.mil/interactive-reports/gender-equality/index.html](https://www.sigar.mil/interactive-reports/gender-equality/index.html "https://www.sigar.mil/interactive-reports/gender-equality/index.html")
9. https://www.theguardian.com/us-news/2021/sep/29/frank-mckenzie-doha-agreement-trump-taliban < Surprising no one.

