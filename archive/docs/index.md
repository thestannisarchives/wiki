# The Stannis Archives

On December 19th, 2020, a neoconservative schizophrenic took to digging as deep as possible to find sources which would exonerate the United States for every single one of their foreign policy decisions — ever. He would spend years compiling a list of his findings onto an internet chatroom, before converting to Stalinism and abandoning the project. Despite his insanity, he undeniably had a talent for research. 

A lot of this information is stuff you can't easily Google: it's no secret that academia and the press have historically had a left-wing slant, one which tends to be skeptical of the Pentagon. The US foreign policy establishment, for their part, [often put little stock](https://www.foreignaffairs.com/united-states/iraq-war-vietnam-syndrome-leaders) in the importance of public opinion. Combining all this with a long-standing tradition of secrecy, has created an environment in which it is difficult to find sources which explain the rationale behind US foreign policy in specific terms. 

This wiki has a lot of information and a lot of links. Carefully look through it, and judge for yourself what is or isn't correct. We've taken to organizing everything by subject, to make it as easy as possible for you to find a subject, find the sources you want, and cite them wherever you want. 

<hr>

_The contents of this wiki are licensed under [Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa]._

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg


